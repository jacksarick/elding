// Set array
var value = [];

// For loop adds item to array
for (var i = 0; i <= 20; i++) {
	value[i] = i;
}

// Print all values with <br> tags
console.log(value.join("<br>"));