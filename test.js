function try_mod(name) {
	try {
		var module = require(name);
		return module;
	}

	catch (err){
		console.log("Missing module: " + name);
		return undefined;
	}
}

try_mod("marked");
try_mod("colors");
try_mod("http");
try_mod("fs");
var config = try_mod("./config.json");

if (config) {
	var test_keys = ["index", "css", "log_file", "error.e_403", "error.e_404", "error.e_500"];
	for (var i = 0; i <= test_keys.length - 1; i++) {
		if(config[test_keys[i]] == undefined){
			console.log("Config is missing critical piece: " + test_keys[i]);
		}
	}
}